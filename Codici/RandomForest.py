#!/usr/bin/env python3


#%%
import numpy as np
from matplotlib import pyplot as plt
#import h5py

import time, os, re, sys

from sklearn import utils
from sklearn import model_selection 
from sklearn import preprocessing
from sklearn import metrics

from sklearn.model_selection import train_test_split, ParameterGrid

# K-fold validation
from sklearn.model_selection import RepeatedStratifiedKFold

# PCA
from sklearn.decomposition import PCA

# Scoring
# from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score
# from sklearn.metrics import roc_curve, auc
from sklearn import metrics

# Classificatori
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier

import imblearn

from sklearn.pipeline import make_pipeline, make_union
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.impute import SimpleImputer
from sklearn.compose import ColumnTransformer, make_column_transformer, make_column_selector
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import FunctionTransformer
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score, make_scorer
from imblearn.over_sampling import SMOTE
import pickle

#%% Settings

Classificatore = "RandomForest"
metrics = ['accuracy', 'f1_macro', 'roc_auc']
scoring = {"f1_macro": "f1_macro", "Accuracy": make_scorer(accuracy_score), "AUC":"roc_auc"}

print(f"Welcome to the party! The program {Classificatore} is starting...")
print(f"Metrics to consider = {metrics}")


path_to_save = r"../local-results/"


thrEV = .95     # Soglia Explained Variance per pruning componenti

testEvts = None    # Numero di eventi del TeS da usare per il test (None = tutti)
n_jobs = -1        # Numero di processi paralleli (-1 = Numero di cores)

# K-fold
n_splits = 5    # Numero folder
n_repeats = 2   # Numero ripetizioni


#%% Data prep

dati = np.load("../Dataset/dati.npz")
X = dati["X"]
y = dati["y"]

# transform the dataset
oversample = SMOTE()
X, y = oversample.fit_resample(X, y)

scaler = preprocessing.StandardScaler()

scaler.fit(X)
X_scaled = scaler.transform(X)

# Applico la PCA
pca = PCA()
pca.fit(X_scaled)
X_pca = pca.transform(X_scaled)


# Estraggo le Explained Variances
EV = pca.explained_variance_ratio_
cumEV = np.cumsum(EV)


# Stabilisco che componenti tenere per il pruning
# Le componenti sono già ordinate
idxLastFeature = np.sum(cumEV <= thrEV)
print(f"To have {thrEV * 100} % of Explaied Variance, we need {idxLastFeature} features ")


# Pruno tenendo solo le prime componenti più informative
X_pruned = X_pca[:,:idxLastFeature]     # Pruno




#%% Kfold
rskf = RepeatedStratifiedKFold(n_splits = n_splits, 
                               n_repeats = n_repeats, 
                               random_state = 13)
nStepKF = rskf.get_n_splits(X, y)



#%%% Preparo la griglia
model =  RandomForestClassifier()

# # Grid search
# n_estimators = [1]      # quanti alberi
# criterion = [ "gini"] #"log_loss"]                     # criterio di valutazione degli split
# min_samples_leaf = [1]                 # minimo numero di pattern in una foglia
# max_depth = [1, 3]              # massima profondità dell'albero

# Grid search
n_estimators = [1, 10, 50, 75, 100, 125, 150, 200, 250, 300]      # quanti alberi
criterion = [ "gini", "entropy",]# "log_loss"]                     # criterio di valutazione degli split
min_samples_leaf = [1,0.01,0.025]                  # minimo numero di pattern in una foglia
max_depth = [1, 3, 5, 7, 9, 11, 13]                # massima profondità dell'albero

# >>>
# >>>
# Dizionario per la grid search
params = {"n_estimators": n_estimators,
          "criterion": criterion,
          "min_samples_leaf": min_samples_leaf,
          "max_depth" : max_depth
          }


t = time.time()
grid_pipe = GridSearchCV(model,
                        param_grid=params,
                        cv=rskf,
                        verbose=10,
                        scoring=scoring,
			            refit = "Accuracy",
                        n_jobs=n_jobs)

print(f"--> Starting gridsearch...")
grid_result = grid_pipe.fit(X_pruned, y)


print(f"Best parameter--> {grid_pipe.best_params_}")
print(f"Best score --> {grid_pipe.best_score_}")

file_path = path_to_save + 'gr_' + Classificatore + '_' + 'prova' + '.pkl'

print(f"Time elapsed -->: {time.time()-t:.2f} s")


with open(file_path, 'wb') as file:
    pickle.dump(grid_result, file)
    print(f'Object successfully saved to "{file_path}"')



