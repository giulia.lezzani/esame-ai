#!/usr/bin/env python3
#%%
import numpy as np
from matplotlib import pyplot as plt

import time, os, re, sys

from sklearn import utils
from sklearn import model_selection 
from sklearn import preprocessing
from sklearn import metrics

from sklearn.model_selection import train_test_split, ParameterGrid

# K-fold validation
from sklearn.model_selection import RepeatedStratifiedKFold

# PCA
from sklearn.decomposition import PCA

#Scoring
from sklearn import metrics

#For resampling
from imblearn.over_sampling import SMOTE

from sklearn.metrics import accuracy_score, make_scorer

# Classificatori
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier

# Grid search
from sklearn.model_selection import GridSearchCV

# For saving
import pickle

#%% Settings

Classificatore = "DecisionTree"
metrics = ['accuracy', 'f1_macro', 'roc_auc']
scoring = {"f1_macro": "f1_macro", "Accuracy": make_scorer(accuracy_score), "AUC":"roc_auc"}

print(f"Welcome to the party! The program {Classificatore} is starting...")
print(f"Metrics to consider = {metrics}")


path_to_save = r"../local-results/"


path_to_save = r"../local-results/"


thrEV = .95     # Soglia Explained Variance per pruning componenti

testEvts = None    # Numero di eventi del TeS da usare per il test (None = tutti)
n_jobs = -1        # Numero di processi paralleli (-1 = Numero di cores)

# K-fold
n_splits = 5    # Numero folder
n_repeats = 2   # Numero ripetizioni

#%% Data prep

dati = np.load("../Dataset/dati.npz")
X = dati["X"]
y = dati["y"]

# transform the dataset
oversample = SMOTE()
X, y = oversample.fit_resample(X, y)

scaler = preprocessing.StandardScaler()

scaler.fit(X)
X_scaled = scaler.transform(X)

# Applico la PCA
pca = PCA()
pca.fit(X_scaled)
X_pca = pca.transform(X_scaled)


# Estraggo le Explained Variances
EV = pca.explained_variance_ratio_
cumEV = np.cumsum(EV)


# Stabilisco che componenti tenere per il pruning
# Le componenti sono già ordinate
idxLastFeature = np.sum(cumEV <= thrEV)
print(f"To have {thrEV * 100} % of Explaied Variance, we need {idxLastFeature} features ")


# Pruno tenendo solo le prime componenti più informative
X_pruned = X_pca[:,:idxLastFeature]     # Pruno

#%% Kfold
rskf = RepeatedStratifiedKFold(n_splits = n_splits, 
                               n_repeats = n_repeats, 
                               random_state = 13)
nStepKF = rskf.get_n_splits(X, y)

#%%% Preparo la griglia
model =  DecisionTreeClassifier()

# >>>
# Grid search
criterion = ["log_loss", "gini", "entropy",]       # criterio di valutazione degli split
min_samples_leaf = [1, 5, 10, 25, 50, 100]         # minimo numero di pattern in una foglia
max_depth = [1, 3, 5, 10, 20, 30, 50]      # massima profondità dell'albero
min_samples_split = [2, 5, 10, 20]                 # Numero minimo di pattern per separare una foglia


# >>>
# Dizionario per la grid search
params = {"criterion": criterion,
             "min_samples_leaf": min_samples_leaf,
             "max_depth": max_depth,
             "min_samples_split": min_samples_split}




t = time.time()
grid_pipe = GridSearchCV(model,
                        param_grid=params,
                        cv=rskf,
                        verbose=10,
                        scoring=scoring,
			            refit = "Accuracy",
                        n_jobs=n_jobs)

print(f"--> Starting gridsearch...")
grid_result = grid_pipe.fit(X_pruned, y)


print(f"Best parameter--> {grid_pipe.best_params_}")
print(f"Best score --> {grid_pipe.best_score_}")
print(f"Time elapsed -->: {time.time()-t:.2f} s")

#%% Data saving

file_path = path_to_save + 'gr_' + Classificatore + '_' + 'prova' + '.pkl'


with open(file_path, 'wb') as file:
    pickle.dump(grid_result, file)
    print(f'Object successfully saved to "{file_path}"')


