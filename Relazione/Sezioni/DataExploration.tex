\section{Preparazione dei dati}

Prima di poter procedere con l'addestramento dei classificatori utilizzando il dataset, è fondamentale attraversare una fase di \emph{pre-processing}. Durante questa fase, vengono eseguite diverse operazioni cruciali per assicurare che i dati siano adeguatamente preparati per l'analisi. Ciò può includere, ad esempio, la rimozione di dati mancanti o \emph{outliers}, la normalizzazione delle \emph{feature}, la gestione della dimensionalità e altre trasformazioni necessarie per garantire che il dataset sia affidabile e adatto per l'addestramento dei modelli di \emph{machine learning}. 

\subsection{Esplorazione dei dati}

Il dataset utilizzato è stato acquisito dal sito \cite{kaggle:dataset}. Esso comprende un totale di \num{839714} campioni, ciascuno dei quali è caratterizzato da \num{31} variabili. Tuttavia, data la presenza di valori nulli in molte di queste variabili, è stata effettuata una selezione basata sulle indicazioni fornite dall'autore del dataset, come riportato nella tabella~\ref{tab:featuresSel}. Di conseguenza, il numero di variabili è stato ridotto a \num{14}. È importante sottolineare che sono ancora presenti dati mancanti, pertanto sono stati eliminati i campioni che contenevano almeno un valore nullo. Questo processo ha portato a una diminuzione del dataset da \num{839714} a \num{820572} campioni, con una riduzione di circa il \SI{2}{\percent}.

\begin{table}
    \centering
    \caption{\emph{Features} selezionate}
    
    \label{tab:featuresSel}
    
    \begin{tabular}{llc}
    \toprule 
    Nome \emph{feature}   &  Significato fisico  & Unità di misura \\
    \midrule
    a     & Semi asse maggiore                           & \si{\astronomicalunit} \\
    e     & Eccentricità                                 & -                      \\
    om    & Longitudine del nodo ascendente              & -                      \\
    i     & Inclinazione rispetto al piano ellittico x-y & \si{\degree}           \\
    w     & Argomento del perielio                       & -                      \\
    q     & Distanza al perielio                         & \si{\astronomicalunit} \\
    ad    & Distanza all'afelio                          & \si{\astronomicalunit} \\
    pery  & Periodo orbitale                             & years                   \\
    H     & Parametro di magnitudine assoluta            & - \\
    neo   & Near of earth object (N/Y)                   & - \\
    moid  & Minima distanza all'intersezione dell'orbita & \si{\astronomicalunit} \\
    n     & Moto medio                                   & $\si{\degree}/\text{day}$  \\
    per   & Periodo orbitale                             & day \\
    ma    & Anomalia media                               & \si{\degree} \\

    \bottomrule
    \end{tabular}
    
\end{table}
    

All'interno del dataset, la \emph{ground truth} è identificata nella colonna \emph{pha}, la quale può assumere uno dei due valori: \emph{Y} se l'asteroide è considerato potenzialmente pericoloso e \emph{N} se non lo è. L'analisi delle frequenze relative a questi due valori rivela che il dataset è estremamente sbilanciato. Come evidenziato chiaramente nel grafico~\ref{fig:PieChart}, la stragrande maggioranza degli eventi corrisponde ad asteroidi non pericolosi. Questo riflette la realtà, dato che gli eventi di asteroidi potenzialmente pericolosi per la Terra sono fortunatamente rari.

Nella sezione successiva, verrà spiegato come affrontare questa specifica caratteristica del dataset. È importante sottolineare che in contesti come questo, con una distribuzione così sbilanciata, è essenziale adottare un approccio ponderato. Ad esempio, un classificatore che prevede costantemente "l'asteroide non è pericoloso" può raggiungere un'accuratezza apparentemente elevata del \SI{99.75}{\percent}, ma tale approccio potrebbe non essere pratico né sensato. 

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{Grafici/PieChart.eps}
    \caption{Frequenza delle classi \emph{pha} = \emph{Y} e \emph{pha} = \emph{N}}
    \label{fig:PieChart}
\end{figure}


Dal momento che sono presente alcune colonne categoriche (Yes/No), sono state trasformate in valori numerici utilizzando le classe apposita di sklearn, LabelEncoder.
\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{Grafici/DistrFeatures.eps}
    \caption{Distribuzioni delle \emph{features}}
    \label{fig:DistrFeatures}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=1 \textwidth]{Grafici/DistrFeatures_YN.eps}
    \caption{Distribuzioni delle \emph{features} a seconda della classe di appartenenza}
    \label{fig:DistrFeatures_YN}
\end{figure}

Successivamente, il dataset è stato suddiviso in due parti: una contenente le caratteristiche (\emph{features}) e l'altra contenente la \emph{ground truth}. La distribuzione delle \emph{features} è mostrata nella figura~\ref{fig:DistrFeatures}, mentre la figura~\ref{fig:DistrFeatures_YN} presenta l'analisi della distribuzione delle \emph{features} in relazione alla \emph{ground truth}.








\subsection{Gestione di un Dataset Sbilanciato}

Un dataset è definito sbilanciato quando una delle classi di destinazione è notevolmente meno rappresentata rispetto alle altre. Questa situazione è piuttosto comune in vari problemi di classificazione, come ad esempio nella diagnosi di malattie, nell'individuazione di transazioni fraudolente, nella valutazione del tasso di abbandono dei clienti o nella gestione di disastri naturali. Nel contesto di un dataset sbilanciato, la sfida principale consiste nel valutare accuratamente quanto il modello sia in grado di predire correttamente sia la classe maggioritaria che quella minoritaria, poiché esiste il rischio che il classificatore sia incline a compiere previsioni distorte.

In questo scenario, la matrice di confusione diventa uno strumento fondamentale per valutare le prestazioni del modello. Essa fornisce una chiara rappresentazione di quanto bene il modello sia in grado di classificare le diverse categorie, e l'accuratezza generale del modello può essere dedotta proprio da questa matrice di confusione.

Una matrice di confusione è una tabella utilizzata nell'ambito della classificazione nei problemi di \emph{machine learning}. Essa mostra una rappresentazione delle prestazioni di un modello di classificazione confrontando le previsioni del modello con le vere etichette dei dati. La matrice di confusione include quattro categorie principali: vero positivo (VP), vero negativo (VN), falso positivo (FP) e falso negativo (FN). Questi valori consentono di calcolare diverse metriche di valutazione del modello, che aiutano a comprendere quanto bene il modello sia in grado di classificare le diverse classi. 

Vi sono alcuni metodi per affrontare questo problema:

\begin{itemize}
    \item \textbf{Scelta di una metrica di valutazione appropriata}: L'accuratezza di un classificatore rappresenta il rapporto tra il numero totale di predizioni corrette e il numero totale di predizioni effettuate. Questa metrica può essere sufficiente per dataset con classi ben bilanciate, ma non è l'ideale per dataset sbilanciati. Altre metriche da considerare includono la \emph{precision} (precisione), che misura quanto sia accurata la classificazione di una specifica classe, e il \emph{recall} (richiamo), che misura la capacità del classificatore di identificare una classe. Tuttavia, per dataset sbilanciati, la metrica più appropriata è spesso l'\emph{F1-score}. L'\emph{F1-score} rappresenta la media armonica tra la precisione e il \emph{recall}, ed è calcolata come segue:
    \begin{equation*}
        \text{F}_{1}=2 \cdot \frac{\text{precision} \cdot \text{recall}}{\text{precision} + \text{recall}}
    \end{equation*}


    \item \textbf{Ricampionamento}: Questa tecnica può essere utilizzata per sovracampionare la classe minoritaria o sottocampionare la classe maggioritaria. L'obiettivo è ottenere un numero simile di campioni per ogni classe, in modo che il classificatore attribuisca uguale importanza a entrambe le classi.
    
    \item \textbf{SMOTE (Synthetic Minority Oversampling Technique)}: SMOTE \cite{Chawla_2002} è un'altra tecnica di sovracampionamento per la classe minoritaria. A differenza dell'approccio di duplicazione dei record della classe minoritaria, SMOTE genera nuove istanze dai dati esistenti. In breve, SMOTE esamina le istanze della classe minoritaria e, utilizzando i k vicini, seleziona casualmente un vicino per generare una nuova istanza all'interno dello spazio delle feature.
    
    \item \textbf{BalancedBaggingClassifier}: Quando si utilizza un classificatore tradizionale per affrontare un dataset sbilanciato, il modello tende a favorire la classe di maggioranza a causa della sua predominanza nei dati. Il BalancedBaggingClassifier, simile a un classificatore di scikit-learn, integra un bilanciamento aggiuntivo. Questo approccio prevede un passaggio aggiuntivo per bilanciare l'insieme di addestramento al momento dell'addestramento attraverso un determinato campionatore (\emph{sampler}).
    
\end{itemize}


Inizialmente, è stata utilizzata una metrica diversa dalla precisione per la classificazione, ossia l'\emph{F1-score}, mantenendo intatto il dataset originale. Tuttavia, questa scelta portava a un punteggio F1 del \SI{50}{\percent}, corrispondente quindi alle performance di un classificatore casuale. Dopo vari tentativi di sovracampionamento e sottocampionamento, è stata presa la decisione di adottare la tecnica SMOTE. Infatti quest'ultima è ampiamente adottata per affrontare queste problematiche ed è più sofisticata rispetto alle tecniche di ricampionamento basilari.
Nella figura~\ref{fig:SMOTED}, vengono presentate le distribuzioni delle \emph{feature} prima e dopo il ricampionamento, mentre nella figura~\ref{fig:SMOTED_YN}, sono illustrate le distribuzioni delle \emph{feature} per i pattern appartenenti alla classe 'pha' = 'Y' prima e dopo il ricampionamento. Si nota che le distribuzioni non sono identiche, ma l'andamento dopo il ricampionamento segue quello originale. Attraverso questa operazione, il numero di pattern è aumentato del \SI{99.51}{\percent}, passando da \num{820572} a \num{1637114}. Questo ha portato a una rappresentazione equilibrata delle classi, con il \SI{50}{\percent} dei pattern appartenenti alla classe \emph{pha} = \emph{Y} e il restante \SI{50}{\percent} alla classe \emph{pha} = \emph{N}.


\begin{figure}
    \centering

    \includegraphics[width = \textwidth]{Grafici/SMOTED.eps}

    \caption[]{Distribuzioni delle \emph{features} prima e dopo il ricampionamento}
    \label{fig:SMOTED}
\end{figure}


\begin{figure}
    \centering

    \includegraphics[width = \textwidth]{Grafici/SMOTED_YN.eps}

    \caption[]{Distribuzioni delle \emph{features} prima e dopo il campionamento per la classe minoritaria}
    \label{fig:SMOTED_YN}
\end{figure}


\subsection{PCA e data scaling}


Successivamente, sono state eseguite le fasi di \emph{data scaling}, \emph{data transformation} e \emph{pruning}.

Il \emph{data scaling} rappresenta una fase di normalizzazione dei dati, il cui obiettivo è attribuire un peso uniforme a ciascuna \emph{feature} durante la fase di classificazione, indipendentemente dai valori assoluti che esse assumono. Per questo progetto, è stata utilizzata la tecnica di \emph{data standardization}, che consiste in uno \emph{scaling} basato su medie e deviazioni standard:

\begin{equation*}
\text{f}\text{i} \to \frac{\text{f}\text{i}-\bar{\text{f}}}{\sigma_\text{f}}
\end{equation*}

In questo modo, se $\text{f}_\text{i}$ segue una distribuzione gaussiana, la sua standardizzazione produrrà una distribuzione gaussiana con media zero e varianza unitaria.
È stato utilizzato questo tipo di \emph{scaler} poiché è quello più indicato per eseguire al meglio al \emph{Principal Component Analysis}. Il risultato dello \emph{scaling} si può osservare nella figura~\ref{fig:scaling}.


\begin{figure}
    \centering

    \includegraphics[width = \textwidth]{Grafici/scaling.eps}

    \caption[]{Distribuzione delle \emph{features} prima e dopo lo \emph{scaling}}
    \label{fig:scaling}
\end{figure}

La \emph{data transformation} implica la possibilità di effettuare una trasformazione da una base delle \emph{features} di input a una seconda base, potenzialmente più informativa. In generale, una base ideale per la classificazione è quella in cui i pattern mostrano la massima dispersione ($\sigma$) per ciascuna feature. Questo rende più agevole definire regioni distinte nel dataset e, di conseguenza, effettuare la classificazione, anche utilizzando algoritmi non parametrici. La tecnica principale in questo contesto è nota come PCA (\emph{Principal Component Analysis}). Dal punto di vista pratico, questa fase può essere immaginata come una rotazione dello spazio delle \emph{features} (figura~\ref{fig:PCA}). Ad esempio, se i dati si distribuiscono lungo un segmento, occupando la prima metà con una classe $\omega_1$ e la seconda metà con una classe $\omega_2$, allora una trasformazione di base ottimale è quella che allinea il segmento lungo l'asse x. In questo modo, si massimizza la dispersione dei dati.


% \begin{figure}
%     \centering
%     \includegraphics[width = \textwidth, center]{Grafici/PCA.jpg}
%     \caption[]{Ciao}
%     \label{fig:PCA}
% \end{figure}
\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{Grafici/PCA.jpg}
    \caption{Distribuzione dei pattern all'interno del \emph{training set}}
    \label{fig:PCA}
\end{figure}



La PCA opera sulla matrice di covarianza delle \emph{features}, che è una matrice con le varianze delle singole \emph{features} sulla diagonale e le covarianze tra di esse negli altri elementi. Il nucleo della PCA è il processo di diagonalizzazione della matrice di covarianza. La figura~\ref{fig:mat_covarianza} presenta la matrice di covarianza calcolata sui dati precedentemente riscaliati. È evidente che alcune \emph{feature} mostrano una forte correlazione, il che indica che non contribuiscono con informazioni aggiuntive e risultano poco utili per la classificazione. La successiva applicazione della PCA affronterà questa problematica. 



\begin{figure}
    \centering

    \includegraphics[width = 0.8\textwidth]{Grafici/mat_covarianza.eps}

    \caption[]{Matrice di covarianza}
    \label{fig:mat_covarianza}
\end{figure}

La diagonalizzazione inizia con il calcolo degli autovalori della matrice, che sono le radici dell'equazione caratteristica. Da ciascun autovalore, è possibile calcolare la cosiddetta \emph{explained variance} (EV), definita come:

\begin{equation*}
\text{EV}(\lambda_\text{i}) = \frac{\lambda_\text{i}}{\sum_{\text{j}} \lambda_\text{j}}
\end{equation*}

Questa misura quantifica quanto ciascuna delle componenti principali, cioè delle \emph{feature} nello spazio trasformato, contribuisca a spiegare le differenze tra i vari pattern.

A questo punto, è possibile eseguire un \emph{pruning}, ovvero una selezione delle \emph{features} basata sul loro contributo informativo. Conviene mantenere le prime K \emph{features} in base al nuovo ordinamento. Il valore di K può essere determinato esaminando la distribuzione degli autovalori ordinati e la loro distribuzione cumulativa, impostando una soglia arbitraria su quest'ultima; per questo progetto, è stata scelta una soglia del \SI{95}{\percent}, che riduce il numero delle \emph{feature} (K) a 7, come mostra la figura~\ref{fig:EV}


\begin{figure}
    \centering

    \includegraphics[width = 0.6\textwidth]{Grafici/EV.eps}

    \caption[]{Explained Variance}
    \label{fig:EV}
\end{figure}


