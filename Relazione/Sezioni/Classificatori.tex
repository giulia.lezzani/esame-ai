\section{Classificatori}

I classificatori rappresentano modelli di \emph{machine learning}  che apprendono dalle informazioni presenti nei dati di addestramento per effettuare previsioni o classificazioni su nuovi dati. Il processo di addestramento implica l'utilizzo di algoritmi dedicati a estrarre modelli e relazioni significative dai dati di addestramento, consentendo così di formulare previsioni su dati non ancora osservati. Tra gli algoritmi utilizzati in questo progetto figurano: \emph{Decision Tree}, \emph{Random Forest}, \emph {K-Nearest Neighbors} (KNN), \emph{Adaboost}, \emph{Feed-Forward Fully-Connected Multi-Layerd Perceptron Neural Network}. Per ciascun classificatore sono stati testati diversi iperparametri, al fine di identificare quelli che ottengono le performance migliori. Questa sezione illustrerà il processo seguito e presenterà i risultati conseguiti.


\subsection{Approccio e metodologia Generale}


Al fine di individuare la migliore combinazione di iperparametri per ciascun classificatore, è stata eseguita un'analisi di \emph{grid-search}. In questa fase, dopo aver definito quali iperparametri variare e aver specificato una lista di possibili valori per ciascuno di essi, sono state esplorate tutte le combinazioni possibili grazie all'utilizzo della classe GridSearchCV di scikit-learn.

GridSearchCV è uno strumento fondamentale per ottimizzare gli iperparametri di un modello mediante una ricerca esaustiva di configurazioni specifiche. Questa classe esegue una ricerca su una "griglia" di combinazioni di iperparametri specificati, valutando le prestazioni del modello su ciascuna combinazione mediante l'utilizzo della \emph{cross-validation}.

Il parametro cv di GridSearchCV specifica il numero di \emph{fold} o pieghe da utilizzare nella \emph{cross-validation}. %Ad esempio, se cv$=5$, si adotta una \emph{cross-validation} a \num{5} \emph{fold}, suddividendo il set di dati in \num{5} parti uguali. Il modello viene addestrato \num{5} volte, ciascuna volta utilizzando una combinazione diversa di \emph{4} parti per l'addestramento e la parte rimanente per la valutazione delle prestazioni.
L'utilizzo della \emph{cross-validation} è cruciale per ottenere stime affidabili delle prestazioni del modello, riducendo il rischio di \emph{overfitting} o \emph{underfitting} su un particolare subset di dati di addestramento. Nello specifico, è stata adottata la \emph{k-fold cross-validation} in versione stratificata, denominata RepeatedStratifiedKFold, garantendo una suddivisione dei dati che mantiene la distribuzione delle classi originale in ciascun \emph{fold}.
Questo processo è stato iterato un numero specificato di volte, assicurando stime robuste delle performance del modello. Impostando RepeatedStratifiedKFold(n\_splits=k, n\_repeats=r), si è garantito che la suddivisione dei dati avvenisse in k fold per ogni ripetizione, assicurando che ciascun fold fosse utilizzato almeno una volta come set di test durante l'intero processo di validazione incrociata. In questo contesto, è stata scelta la configurazione k$=5$, distribuendo il $80\%$ dei dati per l'addestramento e il restante $20\%$ per la valutazione delle prestazioni, una scelta comune in questo tipo di analisi.

GridSearchCV si è rivelato uno strumento fondamentale nella selezione del classificatore ottimale, fornendo indicazioni preziose basate su diverse metriche di valutazione. Le metriche che sono state scelte per valutare le prestazioni del modello includono:

\begin{itemize}
     


    \item \textbf{Accuracy}: La percentuale di predizioni corrette rispetto al totale. In un dataset bilanciato, l'\emph{accuracy} è particolarmente rilevante, essendo adeguata per valutare le prestazioni generali del classificatore.

    \item \textbf{F1 Macro}: Questa metrica tiene conto sia della precisione che della \emph{recall}, fornendo una valutazione complessiva delle prestazioni del modello su tutte le classi. Anche se l'\emph{accuracy} è appropriata, la \emph{F1 Macro} offre ulteriori informazioni sulla capacità del modello di gestire le diverse classi in modo equilibrato.

    \item \textbf{ROC AUC (Area Under the Receiver Operating Characteristic Curve)}: Questa metrica valuta la capacità del modello di distinguere tra le classi, considerando il trade-off tra la sensibilità (\emph{True Positive Rate}) e la specificità (\emph{True Negative Rate}). Anche se il dataset è bilanciato, la ROC AUC può fornire dettagli sulla performance del modello in situazioni di squilibrio.

\end{itemize}

In considerazione del fatto che il dataset è stato bilanciato grazie al \emph{resampling}, l'\emph{accuracy} risulta più che sufficiente per valutare le prestazioni generali del modello. Tuttavia, sono state mantenute anche le altre metriche come misure aggiuntive di sicurezza e per ottenere una visione più approfondita delle capacità del classificatore in diverse situazioni.

Dopo aver identificato i classificatori ottimali tramite la \emph{grid-search}, il modello è stato sottoposto a un processo di \emph{refitting}, seguito dall'analisi di diverse metriche, tra cui:

\begin{itemize}
    \item \textbf{Accuracy}: Valutazione generale delle \emph{performance} del modello, rappresentando la percentuale di previsioni corrette rispetto al totale.
    \item \textbf{Precisione}: Misurazione della proporzione di istanze positive predette correttamente rispetto al totale delle istanze positive previste.
    \item \textbf{Recall}: Valutazione della proporzione di istanze positive predette correttamente rispetto al totale delle istanze positive effettive.
    \item \textbf{F1}: Metrica bilanciata tra precisione e \emph{recall}, fornendo una valutazione complessiva delle prestazioni del modello.
    \item \textbf{Matrice di Confusione}: Rappresentazione dettagliata delle previsioni del modello, evidenziando veri positivi, falsi positivi, veri negativi e falsi negativi.
    \item \textbf{ROC Curve e AUC}: Visualizzazione del \emph{trade-off} tra sensibilità e specificità del modello, con l'AUC che quantifica globalmente le performance attraverso l'area sotto la curva ROC.
\end{itemize}

È essenziale notare che queste analisi sono state condotte successivamente a un accurato \emph{pre-processing} dei dati, come descritto nelle sezioni precedenti, garantendo una preparazione adeguata del dataset prima dell'addestramento e della valutazione del modello.



\subsection{Decision Tree}

Un albero decisionale, o \emph{decision tree}, è un modello di \emph{machine learning}  utilizzato per problemi di classificazione e regressione. Si basa su una struttura ad albero composta da nodi decisionali e foglie.

\begin{itemize}
 
    \item \textbf{Nodi Decisionali (Split Nodes)}: Rappresentano decisioni basate su attributi/\emph{features} dei dati. Ad ogni nodo decisionale, l'albero si divide in rami distinti in base a una condizione logica.

    \item \textbf{Rami (Branches)}: Collegano i nodi decisionali alle possibili esecuzioni basate sulla condizione logica specificata nel nodo.

    \item \textbf{Foglie (Leaf Nodes}): Rappresentano le etichette di classificazione o i valori di output in caso di un problema di regressione. Ogni foglia è associata a una decisione finale o a un valore predetto.

\end{itemize}

L'albero decisionale viene costruito in modo ricorsivo, dividendo il dataset in base alle condizioni che massimizzano la purezza o riducono l'errore nei nodi figli. Questo processo continua fino a quando viene soddisfatta una condizione di stop, come ad esempio una profondità massima dell'albero o un numero minimo di campioni in una foglia.

Gli alberi decisionali sono apprezzati per la loro interpretabilità, poiché è possibile visualizzare l'albero e comprendere le decisioni prese in ciascun passaggio. Tuttavia, possono essere suscettibili all'\emph{overfitting}, motivo per cui spesso si utilizzano tecniche come la potatura (\emph{pruning}) per semplificare l'albero e migliorare le prestazioni sul set di dati di test.

La \emph{grid-search} ha coinvolto i seguenti iper-parametri:

\begin{itemize}
    \item criterion = ["log\_loss", "gini", "entropy",]  $\to$ funzione di loss per individuare rispetto a quale \emph{feature} splittare
    \item min\_samples\_leaf = [1, 5, 10, 25, 50, 100] $\to$ numero minimo di pattern affinché un nodo possa essere una foglia (nodo termiinale, senza figli)
    \item max\_depth = [1, 3, 5, 10, 20, 30, 50] $\to$ massima profondità \emph{N} dell'albero \emph{k}-dimensionale
    \item min\_samples\_split = [2, 5, 10, 20] $\to$ minimo numero di pattern per poter dividere un nodo intero
\end{itemize}


A seconda della metrica usata, due combinazioni di iperparametri sono state considerate le migliori. Per quanto riguarda le metriche \emph{F1 macro} e \emph{accuracy}, la migliore combinazione è stata:

\begin{itemize}
    \item criterion = entropy
    \item max\_depth = 30
    \item min\_samples\_leaf = 1
    \item min\_samples\_split = 2
\end{itemize}

Che corrisponde all'indice \num{456} della \emph{grid-search}. Sono stati ottenuti i seguenti risultati:

\begin{itemize}
    \item F1 macro = $(99.4324 \pm 0.0069) \%$
    \item Accuracy = $(99.4324 \pm 0.0069) \%$
    \item AUC = $(99.4365 \pm 0.0096) \%$
\end{itemize}

Per quanto riguarda invece la metrica \emph{AUC}, la migliore combinazione di iperparametri è stata:

\begin{itemize}
    \item criterion = gini
    \item max\_depth = 50
    \item min\_samples\_leaf = 100
    \item min\_samples\_split = 2
\end{itemize}

Che corrisponde all'indice \num{456} della \emph{grid-search}. Sono stati ottenuti i seguenti risultati:

\begin{itemize}
    \item f1\_macro = $(99.0502 \pm 0.0224) \%$
    \item Accuracy = $(99.0503 \pm 0.0224) \%$
    \item AUC = $(99.6364 \pm 0.0100) \%$
\end{itemize}

A questo punto entrambi i classificatori sono stati rivalutati. Nella tabella~\ref{tab:decisionTree-res} sono riportati i risultati ottenuti. 


\begin{table}
    \centering
    \caption{Risultati dei migliori classificatori - \emph{Decision Tree}}
    
    \label{tab:decisionTree-res}
    
    \begin{tabular}{ccccc}
    \toprule 
    \multirow{2}*{Idx}    &    Accuracy  &   Precision   &    Recall    &    f1  \\
       & (\%) & (\%)  & (\%) & (\%) \\
    \midrule
    $456$   &   $99.43$  &  $99.06$   &   $99.80$   &  $99.43$ \\
    $332$   &   $99.05$  &  $98.28$   &   $99.85$   &  $99.06$ \\
    \bottomrule
    \end{tabular}
    
    \end{table}

In figura~\ref{fig:DecisionTree-conf-matrix} sono riportate le matrici di confusione per i due classificatori: si può notare come i veri negativi siano molto limitati, che è auspicabile in questa tipologia di problemi.


\begin{figure}
    \centering

    \includegraphics[width = \textwidth]{Grafici/DecisionTree-conf-matrix.eps}

    \caption[]{Matrice di confusione per i migliori classificatori ottenuti con l'algoritmo \emph{Decision Tree}}
    \label{fig:DecisionTree-conf-matrix}
\end{figure}

In figura~\ref{fig:DecisionTree-roc-curve} invece sono riportate le curve ROC: si può notare che l'\emph{area under curve} è del $99.43\%$ nel caso dell'indice $456$ e $99.64\%$ nel caso dell'indice $332$.


\begin{figure}
    \centering

    \includegraphics[width = \textwidth]{Grafici/DecisionTree-roc-curve.eps}

    \caption[]{Curve ROC e \emph{AUC} - \emph{Decision Tree}}
    \label{fig:DecisionTree-roc-curve}
\end{figure}
\subsection{Random Forest}

Una \emph{Random Forest} è un modello di \emph{machine learning}  che si basa sull'idea di creare un insieme (\emph{forest}) di alberi decisionali e combinare le loro previsioni per ottenere un risultato più robusto e preciso. È una tecnica di tipo \emph{ensemble}. La \emph{Random Forest} utilizza una collezione di alberi decisionali come "membri" del suo  \emph{ensemble}. Ogni albero è addestrato su un sottoinsieme casuale dei dati di addestramento e utilizza solo un sottoinsieme casuale delle \emph{features} durante la creazione delle decisioni nei nodi. Per creare diversità tra gli alberi, la \emph{Random Forest} utilizza il sottocampionamento con ripetizione (\emph{bootstrap sampling}). Questo significa che ogni albero viene addestrato su un campione casuale con sostituzione del dataset di addestramento.
Una volta che tutti gli alberi sono addestrati, la \emph{Random Forest} effettua la previsione combinata attraverso il voto (nel caso della classificazione) o la media (nel caso della regressione) delle previsioni degli alberi.
Le \emph{Random Forest} sono in grado di gestire grandi quantità di dati e molte \emph{features}, e sono abbastanza flessibili da poter essere utilizzate per problemi di classificazione e regressione.

La \emph{grid-search} ha coinvolto i seguenti iper-parametri:

\begin{itemize}
    \item n\_estimators = [1, 10, 50, 75, 100, 125, 150, 200, 250, 300]  $\to$ numero di alberi utilizzati per costruire la foresta
    \item criterion = [ "gini", "entropy"] $\to$ funzione di \emph{loss}, già descritta per il \emph{Decision Tree}
    \item min\_samples\_leaf = [1,0.01,0.025] $\to$ parametro analogo a quello del \emph{Decision Tree}. Il fatto che sia un numero float, indica che è da moltiplicare per un numero totale di elementi del \emph{Training Set}
    \item max\_depth = [1, 3, 5, 7, 9, 11, 13] $\to$ massima profondità degli alberi costruiti
\end{itemize}


A seconda della metrica usata, due combinazioni di iperparametri sono state considerate le migliori. Per quanto riguarda le metriche \emph{F1 macro} e \emph{accuracy}, la migliore combinazione è stata:

\begin{itemize}
    \item criterion = entropy
    \item max\_depth = 13
    \item min\_samples\_leaf = 1
    \item min\_samples\_split = 10
\end{itemize}

Che corrisponde all'indice \num{391} della \emph{grid-search}. Sono stati ottenuti i seguenti risultati:

\begin{itemize}
    \item f1\_macro = $(99.1772 \pm 0.0145) \%$
    \item Accuracy = $(99.1772 \pm 0.0145) \%$
    \item AUC = $(99.6137 \pm 0.0109) \%$
\end{itemize}

Per quanto riguarda invece la metrica \emph{AUC}, la migliore combinazione di iperparametri è stata:

\begin{itemize}
    \item criterion = entropy
    \item max\_depth = 13
    \item min\_samples\_leaf = 1
    \item min\_samples\_split = 300
\end{itemize}

Che corrisponde all'indice \num{399} della \emph{grid-search}. Sono stati ottenuti i seguenti risultati:

\begin{itemize}
    \item f1\_macro = $(99.1729 \pm 0.0.0137) \%$
    \item Accuracy = $(99.1729 \pm 0.0137) \%$
    \item AUC = $(99.6685 \pm 0.0105) \%$
\end{itemize}

A questo punto entrambi i classificatori sono stati rivalutati. Nella tabella~\ref{tab:RandomForest-res} sono riportati i risultati ottenuti. 


\begin{table}
    \centering
    \caption{Risultati dei migliori classificatori - \emph{Random Forest}}
    
    \label{tab:RandomForest-res}
    
    \begin{tabular}{ccccc}
    \toprule 
    \multirow{2}*{Idx}    &    Accuracy  &   Precision   &    Recall    &    f1  \\
       & (\%) & (\%)  & (\%) & (\%) \\
    \midrule
    $391$   &   $99.18$  &  $98.39$   &   $100.00$   &  $99.19$ \\
    $399$   &   $99.17$  &  $98.37$   &   $100.00$   &  $99.18$ \\
    \bottomrule
    \end{tabular}
    
    \end{table}

In figura~\ref{fig:RandomForest-conf-matrix} sono riportate le matrici di confusione per i due classificatori: si può notare come i veri negativi siano molto limitati. 


\begin{figure}
    \centering

    \includegraphics[width = \textwidth]{Grafici/RandomForest-conf-matrix.eps}

    \caption[]{Matrice di confusione per i migliori classificatori ottenuti con l'algoritmo \emph{Random Forest}}
    \label{fig:RandomForest-conf-matrix}
\end{figure}

In figura~\ref{fig:RandomForest-roc-curve} invece sono riportate le curve ROC: si può notare che l'\emph{area under curve} è del $99.60\%$ nel caso dell'indice $391$ e $99.67\%$ nel caso dell'indice $399$.


\begin{figure}
    \centering

    \includegraphics[width = \textwidth]{Grafici/RandomForest-roc-curve.eps}

    \caption[]{Curve ROC e \emph{AUC} - \emph{Random Forest}}
    \label{fig:RandomForest-roc-curve}
\end{figure}

\subsection{K-Nearest Neighbors (KNN)}

Il \emph{K-Nearest Neighbors} (KNN) è un algoritmo di \emph{machine learning}  utilizzato per problemi di classificazione e regressione. La sua logica si basa sul concetto che le istanze simili dovrebbero avere etichette simili (nel caso della classificazione) o valori simili (nel caso della regressione).

Ecco una breve spiegazione del funzionamento di KNN:
Il funzionamento del KNN avviene in diverse fasi:
\begin{enumerate}
    \item \textbf{Fase di Addestramento}: Durante la fase di addestramento, l'algoritmo memorizza il dataset di addestramento.
    \item \textbf{Calcolo della Distanza}: Quando si riceve un nuovo dato da classificare o predire, l'algoritmo calcola la sua distanza rispetto a tutti gli altri dati nel dataset di addestramento. La distanza può essere calcolata utilizzando diverse metriche, come la distanza euclidea o la distanza di Manhattan. 
    \item \textbf{Selezione dei Vicini}: Sulla base delle distanze calcolate, l'algoritmo identifica i \emph{K} punti nel dataset di addestramento che sono più vicini al nuovo dato.

    \item \textbf{Voto (nel caso della Classificazione)}: Se l'algoritmo è utilizzato per la classificazione, i \emph{K} vicini votano per la classe del nuovo dato. La classe più frequente tra i vicini diventa la previsione per il nuovo dato.

    \item \textbf{Media (nel caso della Regressione)}: Se l'algoritmo è utilizzato per la regressione, i \emph{K} vicini contribuiscono con i loro valori. La previsione per il nuovo dato è la media dei valori dei \emph{K} vicini.
\end{enumerate}



Il parametro \emph{K} rappresenta il numero di vicini considerati e può essere scelto in base al problema specifico. KNN è noto come un algoritmo \emph{lazy} perché non costruisce un modello durante la fase di addestramento, ma fa le previsioni direttamente sulla base dei dati di addestramento durante la fase di test.

La \emph{grid-search} ha coinvolto i seguenti iper-parametri:

\begin{itemize}
    \item n\_neighbors = [1, 4, 7, 10, 15, 25, 50]  $\to$ numero di \emph{K} primi vicini considerati
    \item weights = ["uniform", "distance"] $\to$ decidere se pesare tutti i punti in modo uguale o in funzione dell'inverso della distanza
    \item algorithm = ["ball\_tree", "kd\_tree", "brute"] $\to$ algoritmo da utilizzare per il calcolo dei pattern più vicini
    \item leaf\_size = [5, 10, 20, 50 ] $\to$ parametro passato agli oggetti BallTree o KDTree
\end{itemize}

A seconda della metrica usata, due combinazioni di iperparametri sono state considerate le migliori. Per quanto riguarda le metriche \emph{F1 macro} e \emph{accuracy}, la migliore combinazione è stata:

\begin{itemize}
    \item algorithm = ball\_tree
    \item leaf\_size = 5
    \item n\_neighbors = 1
    \item weights = uniform
\end{itemize}

Che corrisponde all'indice \num{0} della \emph{grid-search}. Sono stati ottenuti i seguenti risultati:

\begin{itemize}
    \item f1\_macro = $(99.6185 \pm 0.0068) \%$
    \item Accuracy = $(99.6185 \pm 0.0068) \%$
    \item AUC = $(99.6185 \pm 0.0063) \%$
\end{itemize}

Per quanto riguarda invece la metrica \emph{AUC}, la migliore combinazione di iperparametri è stata:

\begin{itemize}
    \item algorithm = ball\_tree
    \item leaf\_size = 5
    \item n\_neighbors = 50
    \item weights = distance
\end{itemize}

Che corrisponde all'indice \num{13} della \emph{grid-search}. Sono stati ottenuti i seguenti risultati:

\begin{itemize}
    \item f1\_macro = $(99.0386 \pm 0.0132) \%$
    \item Accuracy = $(99.0386 \pm 0.0132= \%$
    \item AUC = $(99.7683\pm 0.0089) \%$
\end{itemize}

A questo punto entrambi i classificatori sono stati rivalutati. Nella tabella~\ref{tab:KNN-res} sono riportati i risultati ottenuti. 


\begin{table}
    \centering
    \caption{Risultati dei migliori classificatori - KNN}
    
    \label{tab:KNN-res}
    
    \begin{tabular}{ccccc}
    \toprule 
    \multirow{2}*{Idx}    &    Accuracy  &   Precision   &    Recall    &    f1  \\
       & (\%) & (\%)  & (\%) & (\%) \\
    \midrule
    $0$   &   $99.62$  &  $99.25$   &   $100.00$   &  $99.62$ \\
    $13$   &   $99.04$  &  $98.12$   &   $100.00$   &  $99.05$ \\
    \bottomrule
    \end{tabular}
    
    \end{table}

In figura~\ref{fig:KNN-conf-matrix} sono riportate le matrici di confusione per i due classificatori: si può notare come i veri negativi siano molto limitati. 


\begin{figure}
    \centering

    \includegraphics[width = \textwidth]{Grafici/KNn-conf-matrix.eps}


    \caption[]{Matrice di confusione per i migliori classificatori ottenuti con l'algoritmo KNN}
    \label{fig:KNN-conf-matrix}
\end{figure}

In figura~\ref{fig:KNN-roc-curve} invece sono riportate le curve ROC: si può notare che l'\emph{area under curve} è del $99.62\%$ nel caso dell'indice $0$ e $99.77\%$ nel caso dell'indice $13$.


\begin{figure}
    \centering

    \includegraphics[width = \textwidth]{Grafici/KNN-roc-curve.eps}

    \caption[]{Curve ROC e \emph{AUC} - KNN}
    \label{fig:KNN-roc-curve}
\end{figure}

\subsection{Adaboost}

\emph{Adaboost}, abbreviazione di \emph{Adaptive Boosting}, è un algoritmo di \emph{machine learning}  utilizzato principalmente per problemi di classificazione. La sua caratteristica distintiva è la capacità di migliorare le prestazioni dei modelli di apprendimento debole, trasformandoli in modelli forti. 
\emph{Adaboost} inizia addestrando un classificatore debole sul set di dati di addestramento. Un classificatore debole è un modello di apprendimento che ha prestazioni appena superiori al caso casuale. L'algoritmo assegna dei pesi a ciascuna istanza del set di dati e gli errori commessi dal classificatore debole vengono ponderati in base a questi pesi. Dopodiché costruisce iterativamente una sequenza di classificatori deboli, ciascuno dei quali si focalizza sui dati che sono stati classificati erroneamente dai classificatori precedenti. Ad ogni iterazione, il peso delle istanze mal classificate viene aumentato, dando più enfasi a quei punti nel successivo addestramento.
Alla fine delle iterazioni, \emph{Adaboost} combina i risultati dei classificatori deboli in un modello forte. I classificatori deboli contribuiscono al voto finale in base alla loro abilità predittiva, con un peso più alto assegnato a quelli che hanno dimostrato una maggiore accuratezza.
Il modello \emph{Adaboost} risultante è un classificatore forte che tiene conto delle debolezze dei classificatori precedenti, concentrandosi sulle istanze difficili da classificare.
Questo algoritmo è particolarmente utile quando si hanno classificatori deboli che sono solo leggermente migliori del caso casuale. Integrando tali classificatori in un modello complessivo, \emph{Adaboost} può produrre un classificatore robusto.

La \emph{grid-search} ha coinvolto i seguenti iper-parametri:

\begin{itemize}
    \item n\_estimators = [1, 10, 50, 100, 150, 200, 300, 500, 600]  $\to$ massimo numero di \emph{weak learner} utilizzati
    \item learning\_rate = [.01, .05, .1, .5, 1, 1.5, 5] $\to$ pero applicato a ciascun classificatore ad ogni \emph{boosting iterazion}
\end{itemize}

A seconda della metrica usata, due combinazioni di iperparametri sono state considerate le migliori. Per quanto riguarda le metriche \emph{F1 macro} e \emph{accuracy}, la migliore combinazione è stata:

\begin{itemize}
    \item algorithm = SAMME.R
    \item learning\_rate = 1
    \item n\_estimators = 600
\end{itemize}

Che corrisponde all'indice \num{44} della \emph{grid-search}. Sono stati ottenuti i seguenti risultati:

\begin{itemize}
    \item f1\_macro = $(98.8857 \pm 0.0137) \%$
    \item Accuracy = $(98.8858 \pm 0.0137) \%$
    \item AUC = $(99.5142 \pm 0.0086) \%$
\end{itemize}

Per quanto riguarda invece la metrica \emph{AUC}, la migliore combinazione di iperparametri è stata:

\begin{itemize}
    \item algorithm = SAMME.R
    \item learning\_rate = 1.5
    \item n\_estimators = 600
\end{itemize}


Che corrisponde all'indice \num{53} della \emph{grid-search}. Sono stati ottenuti i seguenti risultati:

\begin{itemize}
    \item f1\_macro = $(98.8838 \pm 0.0125) \%$
    \item Accuracy = $(98.8840 \pm 0.0125= \%$
    \item AUC = $(99.5295 \pm 0.0078) \%$
\end{itemize}

A questo punto entrambi i classificatori sono stati rivalutati. Nella tabella~\ref{tab:Adaboost-res} sono riportati i risultati ottenuti. 


\begin{table}
    \centering
    \caption{Risultati dei migliori classificatori - \emph{Adaboost}}
    
    \label{tab:Adaboost-res}
    
    \begin{tabular}{ccccc}
    \toprule 
    \multirow{2}*{Idx}    &    Accuracy  &   Precision   &    Recall    &    f1  \\
       & (\%) & (\%)  & (\%) & (\%) \\
    \midrule
    $44$   &    $98.89$  &  $97.87$   &   $99.95$    &  $98.90$ \\
    $53$   &    $98.89$  &  $97.88$   &   $99.94$    &  $98.90$ \\
    \bottomrule
    \end{tabular}
    
    \end{table}

In figura~\ref{fig:Adaboost-conf-matrix} sono riportate le matrici di confusione per i due classificatori: si può notare come i veri negativi siano molto limitati. 


\begin{figure}
    \centering

    \includegraphics[width = \textwidth]{Grafici/Adaboost-conf-matrix.eps}

    \caption[]{Matrice di confusione per i migliori classificatori ottenuti con l'algoritmo \emph{Adaboost}}
    \label{fig:Adaboost-conf-matrix}
\end{figure}

In figura~\ref{fig:Adaboost-roc-curve} invece sono riportate le curve ROC: si può notare che l'\emph{area under curve} è del $99.52\%$ nel caso dell'indice $44$ e $99.54\%$ nel caso dell'indice $53$.


\begin{figure}
    \centering

    \includegraphics[width = \textwidth]{Grafici/Adaboost-roc-curve.eps}

    \caption[]{Curve ROC e \emph{AUC} - \emph{Adaboost}}
    \label{fig:Adaboost-roc-curve}
\end{figure}


\subsection{Feed-Forward Fully-Connected Multi-Layerd Perceptron Neural Network}

Un \emph{Feed-Forward Fully-Connected Multi-Layered Perceptron Neural Network} (abbreviato spesso come MLP) è un tipo di rete neurale artificiale composta da più strati di neuroni, ognuno dei quali è completamente connesso a quelli nei \emph{layer} successivi. 

\begin{itemize}

    \item \textbf{Feed-Forward}: L'informazione si sposta attraverso la rete in una sola direzione, dall'input verso l'output, senza cicli o retroazioni. Questo aspetto è noto come \emph{feed-forward}.

    \item \textbf{Fully-Connected}: Ogni neurone in uno strato è connesso a tutti i neuroni nello strato successivo. Questa connessione completa tra i neuroni è indicata come \emph{fully-connected} o \emph{densely-connected}.

    \item \textbf{Multi-Layered}: La rete è composta da più strati, in genere con uno strato di input, uno o più strati nascosti e uno strato di output. I dati fluiscono attraverso la rete passando attraverso ciascuno di questi strati.

    \item \textbf{Perceptron}: Un \emph{perceptron} è l'unità di base di una rete neurale. Ogni \emph{perceptron} riceve input, applica pesi a questi input, somma i risultati pesati e applica una funzione di attivazione per generare un output.
    
    \item \textbf{Neural Network}: L'aggregato di \emph{perceptrons} e strati compongono la struttura della rete neurale. L'apprendimento avviene attraverso l'aggiustamento dei pesi delle connessioni durante il processo di addestramento.

\end{itemize}

In sintesi, un MLP rappresenta una rete neurale composta da più strati di \emph{perceptrons} interconnessi, dove ciascun perceptron ha connessioni con tutti i \emph{perceptrons} nei \emph{layer} successivi. Questo tipo di architettura consente al MLP di apprendere rappresentazioni complesse e non lineari dei dati, rendendolo particolarmente utile per problemi di classificazione e regressione complessi.


La \emph{grid-search} ha coinvolto i seguenti iper-parametri:

\begin{itemize}
    \item hidden\_layer\_sizes $\to$ configurazione dei layer nascosti, numero di neuroni per ciascun \emph{layer}. Sonos state considerate le seguenti architetture
    \begin{itemize}
        \item 64
        \item 64, 32
        \item 64, 32, 16
        \item 128, 64, 32
        \item 128, 64, 32, 16
        \item 128, 128, 64, 64
    \end{itemize}
    \item activation = ["identity", "logistic", "tanh", "relu"] $\to$ funzione di attivazione per i neuroni. Vale la pena menzionare che la funzione ReLU (Rectified
    Linear Unit), definita come $y = 0$ se $x < 0$, $y = x$ se $x \geq 0$, pur non
    ammettendo derivata continua nell'origine, è spesso utilizzata in questo
    contesto, consentendo di raggiungere ottime performance
    \item learning\_rate\_init = [0.001 ,0.01, 0.1] $\to$ \emph{learning rate} iniziale
    \item learning\_rate = ["constant", "invscaling"] $\to$ il \emph{learning rate} può essere costante ("constant") e fissato ad un certo valore (learning\_rate\_init), oppure scalato in funzione del tempo $\sim \text{t}^{0.5}$.
\end{itemize}

Per mitigare il rischio di \emph{overfitting} del \emph{training set} e promuovere una maggiore generalizzazione del classificatore, oltre alla comune \emph{K-Fold Cross Validation}, è stato implementato un \emph{early stop}. Per fare ciò, è stato estratto il \num{10}\% dei pattern dal \emph{training set} per formare il \emph{validation set}. Tale insieme, sebbene potenzialmente sovrapponibile al \emph{test set}, non è utilizzato durante la fase di addestramento. Tuttavia, al termine di ogni iterazione di \emph{backpropagation} (la procedura di addestramento delle reti neurali), la funzione di \emph{loss} è calcolata sul \emph{validation set}. Nel caso in cui, per oltre \num{15} epoche consecutive, non si verifichi un miglioramento di almeno $1 \times 10^{-4}$, il processo di addestramento viene interrotto. La migliore combinazione di iperparametri per tutte le metriche è stata:

\begin{itemize}
    \item activation = relu
    \item hidden\_layer\_sizes = [128, 128,  64,  64]
    \item learning\_rate\_init = 0.001
    \item learning\_rate = invscaling
\end{itemize}

Che corrisponde all'indice \num{141} della \emph{grid search}. Sono stati ottenuti i seguenti risultati:

\begin{itemize}
    \item f1\_macro = $(99.8314 \pm 0.0231) \%$
    \item Accuracy = $(99.8314 \pm 0.0231) \%$
    \item AUC = $(99.9596 \pm 0.0077) \%$
\end{itemize}

A questo punto il classificatore è stato rivalutato. Nella tabella~\ref{tab:NeuralNetwork-res} sono riportati i risultati ottenuti. 


\begin{table}
    \centering
    \caption{Risultati dei migliori classificatori - \emph{Neural Newtork MPL}}
    
    \label{tab:NeuralNetwork-res}
    
    \begin{tabular}{ccccc}
    \toprule 
    \multirow{2}*{Idx}    &    Accuracy  &   Precision   &    Recall    &    f1  \\
       & (\%) & (\%)  & (\%) & (\%) \\
    \midrule
    $141$   &    $99.86$  &  $99.72$   &   $99.99$    &  $99.86$ \\
    \bottomrule
    \end{tabular}
    
    \end{table}

In figura~\ref{fig:NeuralNetwork-conf-matrix} è riportata la matrice di confusione


\begin{figure}
    \centering

    \includegraphics[width = 0.5\textwidth]{Grafici/NeuralNetwork-conf-matrix.eps}

    \caption[]{Matrice di confusione per i migliori classificatori ottenuti con l'algoritmo \emph{Neural Network MPL}}
    \label{fig:NeuralNetwork-conf-matrix}
\end{figure}

In figura~\ref{fig:NeuralNetwork-roc-curve} invece è riportata la curva ROC: si può notare che l'\emph{area under curve} è del $99.96\%$.


\begin{figure}
    \centering

    \includegraphics[width = 0.5\textwidth]{Grafici/NeuralNetwork-roc-curve.eps}

    \caption[]{Curve ROC e \emph{AUC} - \emph{Neural Network MPL}}
    \label{fig:NeuralNetwork-roc-curve}
\end{figure}
